﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject objToSpawn;
    public GameObject dedede;

    private float timeSpawn;
    public float minTime, maxTime;
    private float dededecounter;
    public float offsetY;

	// Use this for initialization
	public void Initialize()
    {
        timeSpawn = minTime;
        dededecounter = 0;
    }
	
	// Update is called once per frame
	public void MyUpdate ()
    {
        dededecounter += Time.deltaTime;
        if(dededecounter >= 5)
        {
            Bossspawn();
        }
        if(timeSpawn <= 0)
        {
            Spawn();
        }
        else timeSpawn -= Time.deltaTime;
	}

    void Spawn()
    {
        timeSpawn = Random.Range(minTime, maxTime);

        Vector2 spawnpos = this.transform.position;
        spawnpos.y += Random.Range(-offsetY, offsetY);

        Instantiate(objToSpawn, spawnpos, Quaternion.identity);
    }
    void Bossspawn()
    {
        Vector2 spawnboss = this.transform.position;
        Instantiate(dedede, spawnboss, Quaternion.identity);
        dededecounter = 0;
    }
}

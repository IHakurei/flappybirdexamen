﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2 : MonoBehaviour
{
    private float temps = 0;

    public float speed;

    void Update()
    {
        transform.Translate(Vector2.left * speed * Time.deltaTime);
        temps += Time.deltaTime;
        if(temps<=1)
        transform.Translate(Vector2.up * speed * Time.deltaTime);
        else
        transform.Translate(Vector2.down * speed * Time.deltaTime);
        if(temps >= 1.5)
            temps = 0;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == "Boundary")
        {
            Dead();
        }
    }

    void Dead()
    {
        Destroy(this.gameObject);
    }
}

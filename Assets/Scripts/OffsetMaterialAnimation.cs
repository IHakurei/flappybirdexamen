﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffsetMaterialAnimation : MonoBehaviour
{
    private Vector2 offset;
    public float smooth = 1;
    private Renderer rend;
	// Use this for initialization
	void Start ()
    {
        rend = GetComponent<MeshRenderer>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        offset.x += Time.deltaTime * smooth;
        if(offset.x >= 10) offset.x = 0; 

        MaterialPropertyBlock block = new MaterialPropertyBlock();
        rend.GetPropertyBlock(block);

        block.SetVector("_MainTex_ST", new Vector4(0.13f, 1.0f, offset.x, offset.y));

        rend.SetPropertyBlock(block);

        
	}
}
